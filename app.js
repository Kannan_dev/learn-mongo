var mongoose = require('mongoose');
mongoose.connect('mongodb+srv://rbkannan1:adminpwd@learndb-q50r9.mongodb.net/test?retryWrites=true', { useNewUrlParser: true });
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    // we're connected!
    console.log('Connected');
    db.createCollection('users', function (err, res) {
        const userList = [{
            _id: "1",
            name: 'Apple',
            city: "Chennai",
            state: "Tamil Nadu",
            country: "India"
        }, {
            _id: "2",
            name: 'Banana',
            city: "Chennai",
            state: "Tamil Nadu",
            country: "India"
        }, {
            _id: "admin",
            name: 'Admin',
            city: "Chennai",
            state: "Tamil Nadu",
            country: "India"
        }]
        db.collection('users').insertMany(userList, (err, res) => {
            console.log(res)
        })
    })
});